# GéoDataMine

GéoDataMine est un outil simple d'extraction thématique de données OpenStreetMap. Il vous permet facilement de :

* Choisir un __territoire__ (commune, EPCI, département)
* Choisir une __thématique__ (parking à vélo, défibrillateurs, enseignement...)
* Et récupérer les __données issues d'OpenStreetMap__ sur ce thème et sur ce territoire (formats CSV, GeoJSON, Shapefile)

L'outil est disponible à cette adresse : [geodatamine.fr](https://geodatamine.fr)

## Installation

Les dépendances sont les suivantes :

* NodeJS >= 9
* PostgreSQL >= 9.6 et ses extensions PostGIS, HStore, PG_TRGM, Unaccent
* wget
* [osm2pgsql](https://github.com/openstreetmap/osm2pgsql) >= 1.4.0
* [jq](https://stedolan.github.io/jq/)
* GDAL >= 2.1 (et en particulier la commande `ogr2ogr`)
* zip et zipnote
* [osmium-tool](https://github.com/osmcode/osmium-tool) >= 1.11

Installation des dépendances sous Debian 9 :

```bash
sudo apt-get install wget zip jq gdal-bin
cd ~

# Node JS 12
wget https://deb.nodesource.com/setup_12.x -O nodejs.sh
sudo bash nodejs.sh
sudo apt-get install nodejs
rm nodejs.sh

# Osm2PgSQL latest
sudo apt-get install git make cmake g++ libboost-dev libboost-system-dev libboost-filesystem-dev libexpat1-dev zlib1g-dev libbz2-dev libpq-dev libproj-dev lua5.2 liblua5.2-dev
git clone git://github.com/openstreetmap/osm2pgsql.git
cd osm2pgsql
mkdir build && cd build
cmake ..
make
sudo make install
cd ../../
rm -rf osm2pgsql

# Osmium-tool
sudo apt-get install libboost-program-options-dev libbz2-dev zlib1g-dev libexpat1-dev
mkdir work
cd work
git clone https://github.com/mapbox/protozero
cd protozero
mkdir build
cd build
cmake ..
make
sudo make install
cd ../../
git clone https://github.com/osmcode/libosmium
cd libosmium
mkdir build
cd build
cmake ..
make
sudo make install
cd ../../
git clone https://github.com/osmcode/osmium-tool
cd osmium-tool
mkdir build
cd build
cmake ..
make
sudo make install
cd ../../

# PostgreSQL 11
echo "deb http://apt.postgresql.org/pub/repos/apt/ stretch-pgdg main" | sudo tee /etc/apt/sources.list.d/pgdg.list
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update
sudo apt-get install postgresql-11 postgresql-client-11 postgresql-11-postgis-3
```

Téléchargement du code de GéoDataMine :

```bash
git clone https://framagit.org/PanierAvide/GeoDataMine.git
cd GeoDataMine
```


## Configuration

### Paramètres généraux

Les paramètres généraux de configuration du service sont à renseigner dans le fichier `config.json`. Les variables à renseigner sont les suivantes :

* `WORK_DIR` : dossier de travail, dans lequel les fichiers temporaires sont stockés (exemple: `/tmp/osm4odf`)
* `PBF_URL` : URL de téléchargement de l'extrait OpenStreetMap au format PBF (exemple: `http://download.openstreetmap.fr/extracts/europe/france-latest.osm.pbf`)
* `PG_HOST` : nom d'hôte pour accéder à la base de données PostgreSQL (exemple: `localhost`)
* `PG_USER` : nom d'utilisateur pour accéder à la base de données PostgreSQL (exemple: `postgres`)
* `PG_USER` : numéro de port pour accéder à la base de données PostgreSQL (exemple: `5432`)
* `PG_DB_CLEAN` : nom de la base de données qui contiendra les données propres (exemple: `osm4odf`)
* `PG_DB_TMP` : nom de la base de données qui contiendra les données en cours d'import (exemple: `osm4odf_next`)
* `PG_DB_OLD` : nom de la base de données qui contiendra les données précédemment en production (exemple: `osm4odf_prev`)
* `PARALLEL_JOBS` : nombre de tâches à exécuter en parallèle, idéalement 75% des coeurs du processeur (exemple: `6`)
* `CACHE_MEM` : mémoire à allouer lors de l'import de données, en MB, idéalement 75% de la mémoire vive disponible (exemple: `10000`)
* `FLAT_NODES` : activer l'option `--flat-nodes` de l'outil osm2pgsql, utile pour les imports de grosses régions ou de pays (`true` pour activer, `false` pour désactiver)
* `API_BASE_URL` : URL à utiliser dans les fichiers de métadonnées pour accéder à l'API (utile si API derrière un proxy), si absent l'adresse directe de l'API est utilisée (exemple: `https://geodatamine.fr`)
* `BOUNDARY` : métadonnées de la zone chargée en base (exemple: `{ "name": "Ille-et-Vilaine", "ref": "35", "id": "35"}`)
* `LICENSE` : texte de la licence, qui apparaît dans les fichiers exportés (exemple `Données © les contributeurs d’OpenStreetMap`)

### Thèmes

Les thèmes de données sont à configurer dans le dossier `themes/`. Chaque thème est représenté par un fichier `.sql` avec une structure précise :

#### 1ère ligne du fichier

Elle décrit les métadonnées du thème, au format JSON avec un préfixe. On y trouve en particulier le nom en français et anglais.

```sql
--METADATA={ "name:fr": "Le nom en français", "name:en": "Le nom en anglais", "theme:fr": "Le thème", "keywords:fr": [ "mot clé 1", "mot clé 2" ], "description:fr": "Blabla" }
```

Les propriétés à renseigner sont :

* `name:fr` : nom du thème en français
* `name:en` : nom du thème en anglais
* `theme:fr` : thématique du jeu de données, sur la base de la nomenclature Eurovoc ou de la [nomenclature OpenDataFrance](https://docs.google.com/document/d/1oDJsHw3bmABfto6HPgCPG1ztrV3CihuHjcfU8tQpvPc/edit#heading=h.5aw35qsix45f)
* `keywords:fr` : liste de mots clés, sous la forme d'un tableau
* `description:fr` : un bref résumé du jeu de données, ainsi que des clés OSM utilisées pour l'extraction
* `skipGeomCSV` : mettre à `true` pour ne pas ajouter la géométrie lors d'un export CSV (si des colonnes lat/long sont déjà prévues)
* `skipDump` : mettre à `true` si le thème doit être ignoré lors des exports complets de la base

### 2e ligne du fichier

Elle décrit le type de géométrie à interroger pour ce thème. Les types possibles sont `point`, `line`, `polygon`. Il est possible d'en renseigner plusieurs, séparés par une virgule.

```sql
--GEOMETRY=point,polygon
```

### Le reste du fichier

Les lignes suivantes du fichiers contiennent la requête SQL permettant d'extraire les données OpenStreetMap souhaitées. La liste des colonnes et leur ordre sera utilisé pour définir les données attributaires du fichier de sortie. Chaque colonne utilisée doit être préfixée par `t.` (alias pour le nom de la table). La requête est à renseigner une seule fois même si l'on souhaite récupérer différents types de géométries. La table à utiliser dans le `FROM` doit être `<TABLE>` (le motif sera remplacé par l'API par le nom correct). La structure de la table à interroger correspond à ce que vous avez configuré dans le fichier `db/flex_rules.lua` (voir ci-après). La géométrie de l'objet est à représenter avec la balise `<GEOM>`. Selon les paramètres passés par l'utilisateur, `<GEOM>` contiendra soit la géométrie initiale, soit le centroïde de celle-ci. Enfin, la condition de filtrage sur le territoire sélectionné est à renseigner avec `<GEOMCOND>`. Cette balise sera remplacé dynamiquement par le nom des champs appropriés.

```sql
SELECT
	t.osm_id, t.name, t.access, t.tags->'operator' AS operator,
	t.tags->'opening_hours' AS opening_hours,
	t.tags->'defibrillator:location' AS location, <GEOM>
FROM <TABLE>
WHERE <GEOMCOND> AND emergency = 'defibrillator'
```

Des balises optionnelles peuvent également être prévues :
* `<ADM8NAME>` : nom de la commune
* `<ADM8REF>` : code INSEE de la commune
* `<GEOMEMBED>` : la géométrie initiale (EPSG:3857), pour utilisation dans des fonctions type `ST_X / ST_Y` ou `ST_Transform`
* `<OSMID>` : l'identifiant de l'objet d'origine dans OpenStreetMap (exemple : `way/1234`)


## Base de données

La base de données contient les informations issues d'OpenStreetMap, afin de les mettre à disposition de l'API.

### Script d'import

La configuration de la structure des données en bases (tags sous forme de colonnes, type d'objets sous forme d'emprise...) sont à détailler dans le fichier `db/flex_rules.lua`. Des exemples pour ce type de fichier [sont disponibles ici](https://github.com/openstreetmap/osm2pgsql/tree/master/flex-config). Il est recommandé de ne faire apparaître sous forme de colonne dédiée uniquement les tags qui feront partie des filtres des requêtes formulées par l'API.

Une fois les configurations réalisées, exécutez simplement les commandes suivantes :

```bash
cd db/
./run.sh
```


## API

L'API propose au site web client (ou à des connexions directes) de télécharger les données thématiques. Elle interroge la base de données PostgreSQL.

### Installation des dépendances

Pour s'exécuter, l'API nécessite que NodeJS (>= 9) soit installé sur la machine. Pour installer les dépendances, lancez les commandes suivantes :

```bash
cd api/
npm install
```

### Exécution

Pour lancer l'API, il suffit d'exécuter la commande suivante :

```bash
npm run start

# Alternativement, vous pouvez changer le numéro de port avec cette commande
PORT=12345 npm run start
```

### Utilisation

Une fois l'API lancée, vous avez accès aux adresses suivantes :

* [localhost:3000](http://localhost:3000/) : interface utilisateur de requêtage
* [localhost:3000/doc](http://localhost:3000/doc) : documentation de l'API


## Licence

Copyright (c) Adrien Pavie 2019

Le développement initial de cet outil a été financé par [OpenDataFrance](http://www.opendatafrance.net).

Mis à disposition sous licence AGPL, voir [LICENSE](LICENSE.txt) pour le texte complet de la licence.
