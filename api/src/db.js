/**
 * This file manages communication with PostgreSQL database
 */

const { Pool } = require('pg');
const fs = require('fs');
const util = require('util');
const exec = util.promisify(require('child_process').exec);
const CONFIG = require('../../config.json');
const FORMAT_TO_EXT = { "csv": "csv", "shapefile": "shp.zip", "geojson": "geojson", "xlsx": "xlsx" };
const GEOM_TO_COND = { "point": "ST_GeometryType(t.way) = 'ST_Point'", "line": "ST_GeometryType(t.way) = 'ST_LineString'", "polygon": "ST_GeometryType(t.way) IN ('ST_Polygon', 'ST_MultiPolygon')" };

// Create pool of connections
let pool = null;

function createPool() {
	pool = new Pool({
		user: CONFIG.PG_USER,
		host: CONFIG.PG_HOST,
		database: CONFIG.PG_DB_CLEAN,
		port: CONFIG.PG_PORT
	});
}

function isoToday() {
	return (new Date()).toISOString().split("T")[0];
}

function toSlug(text) {
	return text.toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/[ '"_]/g, "-");
}

/**
 * Checks if database is ready to receive requests (running and not being updated)
 */
function whenAvailable(t) {
	t = t || 0;

	if(t === 10) {
		pool = null;
		return Promise.reject(new Error("Database is not available"));
	}

	return new Promise((resolve, reject) => {
		if(fs.existsSync(CONFIG.WORK_DIR+"/db.lock")) {
			setTimeout(() => {
				resolve(whenAvailable(t+1));
			}, 1000);
		}
		else {
			if(pool === null) {
				createPool();
			}

			pool.query("SELECT 1+1")
				.then(() => resolve())
				.catch(() => {
					setTimeout(() => {
						resolve(whenAvailable(t+1));
					}, 1000);
				});
		}
	});
}

/**
 * Retrieve list of available boundaries
 */
exports.getBoundaries = () => {
	return whenAvailable()
	.then(() => {
		return new Promise((resolve, reject) => {
			fs.readFile(CONFIG.WORK_DIR+"/bounds.json", (err, data) => {
				if(err) { return reject(err); }
				return resolve(JSON.parse(data));
			});
		});
	});
};

/*
 * Retrieve info of a specific boundary
 */
exports.getBoundary = (id) => {
	return whenAvailable()
	.then(() => {
		return pool.query(`SELECT osm_id AS id, name, replace(simple_name, ' ', '_') AS simple_name, ref, type FROM boundary b WHERE osm_id = $1`, [ id ])
		.then(res => {
			if(res.rows.length > 0) {
				return res.rows[0];
			}
			else {
				throw new Error("Boundary ID is unknown: "+id);
			}
		});
	});
};

exports.findBoundariesByName = (name) => {
	if(pool === null) {
		createPool();
	}

	name = name.trim();

	// Search by ref
	if(name.match(/^[0-9]+$/)) {
		return pool.query(`SELECT osm_id AS id, name, ref, type FROM boundary b WHERE ref LIKE '${name}%' ORDER BY ref LIMIT 10`)
		.then(res => res.rows);
	}
	// Search by name
	else {
		return pool.query(`SELECT osm_id AS id, name, ref, type FROM boundary b ORDER BY lower(unaccent($1)) <-> b.simple_name LIMIT 10`, [ name ])
		.then(res => res.rows);
	}
};

/**
 * Generate metadata text
 */
function getMetadataText(theme, boundary, format, url) {
	boundary = boundary || CONFIG.BOUNDARY;
	let text = "COLL_NOM;COLL_SIRET;ID;TITRE;DESCRIPTION;THEME;PRODUCTEUR_NOM;PRODUCTEUR_SIRET;COUV_SPAT_MAILLE;COUV_SPAT_NOM;COUV_TEMP_DEBUT;COUV_TEMP_FIN;DATE_PUBL;FREQ_MAJ;DATE_MAJ;MOTS_CLES;LICENCE;NOMBRE_RESSOURCES;FORMAT_RESSOURCES;URL";

	const params = [
		boundary.name,
		boundary.ref,
		`osm-${theme.id}-${boundary.ref || boundary.id}`,
		`${theme["name:fr"]} de ${boundary.name} (OpenStreetMap)`,
		`"${theme["description:fr"].replace(/"/g, '"""')}"`,
		theme["theme:fr"],
		"OpenStreetMap",
		"",
		"Infracommunale",
		boundary.name,
		"2008-01-01",
		isoToday(),
		"",
		"Ponctuelle",
		isoToday(),
		`"scdl;${toSlug(theme["theme:fr"])}${theme["keywords:fr"] ? ";" + theme["keywords:fr"].map(t => toSlug(t)).join(";") : ""}"`,
		"Open Database License-ODBL",
		"1",
		format,
		url
	];

	text += "\n" + params.join(";");
	return text;
}

/**
 * Create SQL request for exporting theme data
 * @param {Object} theme The theme metadata
 * @param {Object} boundary The boundary metadata, or null for whole database export
 * @param {Object} [options] Options
 * @param {string} [options.format] Output file format
 * @param {number} [options.radius] Radius around boundary center for data filtering
 * @param {boolean} [options.aspoint] Geometry will be converted as point, instead full geometry is used
 * @param {string} [options.tablename] If output format is "table", use this option to define the table name
 * @return {string} SQL request to generate wanted data
 */
function getSQLForThemeData(theme, boundary, options) {
	const boundaryId = boundary ? "'" + boundary.id.toString().replace(/'/g, "") + "'" : null;

	// Create condition for filtering
	const geomConds = [];

	if(boundaryId) {
		geomConds.push({ sort: 0, cond: `b.osm_id = ${boundaryId}` });

		const dist = options.radius > 0 ? options.radius * 1000 : null;
		if(dist) {
			geomConds.push({ sort: 2, cond: `ST_DWithin(t.centroid, b.center, ${dist})` });
		}
		else {
			geomConds.push({ sort: 1, cond: "b.way && t.centroid" });
			geomConds.push({ sort: 2, cond: "ST_Intersects(t.centroid, b.way)" });
		}
	}

	const hasAdm8 = theme.sql.includes("<ADM8");
	if(hasAdm8) {
		geomConds.push({ sort: 0, cond: "b2.type = 'admin_8'" });
		geomConds.push({ sort: 1, cond: "b2.way && t.centroid" });
		geomConds.push({ sort: 3, cond: "ST_Intersects(b2.way, t.centroid)" });
	}
	const geomCond = geomConds.sort((a, b) => a.sort - b.sort).map(g => g.cond).join(" AND ");

	// Create geometry column
	let geom = options.aspoint ? "t.centroid AS geom" : "t.way AS geom";
	if(options.format === "csv" && theme.skipGeomCSV) { geom = ""; }
	else if(options.format === "xlsx") {
		if(theme.skipGeomCSV) { geom = ""; }
		else {
			geom = options.aspoint ?
				"ST_X(ST_Transform(t.centroid, 4326)) AS x, ST_Y(ST_Transform(t.centroid, 4326)) AS y"
				: "ST_AsText(ST_Transform(t.way, 4326)) AS wkt";
		}
	}
	else if(options.format === "table") {
		geom = "t.centroid AS geom_center, t.way AS geom_full";
	}

	// Create SQL request
	let request = theme.geomtypes.map(g => (
		theme.sql
		.replace(/<TABLE>/g, `${boundaryId ? 'boundary b, ' : ''}planet_osm_${g} t${hasAdm8 ? ", boundary b2" : ""}`)
		.replace(/<GEOM>/g, g === "point" ? geom.replace(/t\.centroid/g, "t.way") : geom)
		.replace(/<GEOMEMBED>/g, "t.way")
		.replace(/<ADM8NAME>/g, "b2.name")
		.replace(/<ADM8REF>/g, "b2.ref")
		.replace(/<OSMID>/g, g === "point" ? "CONCAT('node/', t.osm_id)" : "CASE WHEN t.osm_id > 0 THEN CONCAT('way/', t.osm_id) ELSE CONCAT('relation/', -t.osm_id) END")
		.replace(/<GEOMCOND>/g, g === "point" ? "<GEOMCONDPT>" : "<GEOMCOND>")
		.replace(/SELECT\s+,/g, "SELECT ")
		.replace(/,\s+FROM/g, " FROM")
	)).join(" UNION ");

	// Common conditional geometry system for GeoJSON/CSV and Shapefile as point
	if(["geojson", "csv", "xlsx", "table"].includes(options.format) || (options.format === "shapefile" && options.aspoint)) {
		request = request
			.replace(/<GEOMCOND>/g, geomCond)
			.replace(/<GEOMCONDPT>/g, geomCond.replace(/t\.centroid/g, "t.way"));
	}
	else if(options.format === "shapefile" && !options.aspoint) {
		request = request
			.replace(/<GEOMCOND>/g, `<GEOMCONDTYPE> AND ${geomCond}`)
			.replace(/<GEOMCONDPT>/g, `<GEOMCONDTYPE> AND ${geomCond.replace(/t\.centroid/g, "t.way")}`);
	}

	// Prepend CREATE TABLE if table format
	if(options.format === "table") {
		request = `CREATE TABLE ${options.tablename} AS ${request}`;
	}

	return request;
}

/**
 * Create export files using given SQL request
 * @param {Object} theme The theme metadata
 * @param {Object} boundary The boundary metadata, or null for whole database export
 * @param {string} request The SQL request to retrieve data
 * @param {Object} [options] Options
 * @param {string} [options.format] Output file format
 * @param {boolean} [options.aspoint] Geometry will be converted as point, instead full geometry is used
 * @param {boolean} [options.metadata] Add a metadata file (SCDL format)
 * @param {string} [options.url] Original URL used for getting this dataset
 * @param {string} [options.destinationFolder] Destination folder (defaults to tmp directory set in config.json)
 * @return {Promise} Resolves on output file
 */
function createDataFile(theme, boundary, request, options) {
	const filename = `${theme.id}${boundary ? boundary.id.toString() : "dump"}${Date.now()}_${options.format}`;
	const baseFilePath = `${CONFIG.WORK_DIR}/${filename}`;

	// Generate metadata + license file content
	const metadataFile = baseFilePath + "_metadata.csv";
	const licenseFile = baseFilePath + "_license.txt";
	if(options.metadata) {
		const metadataTxt = getMetadataText(theme, boundary, options.format, options.url);
		fs.writeFileSync(metadataFile, metadataTxt);

		fs.writeFileSync(licenseFile, CONFIG.LICENSE);
	}

	/*
	 * Export commands
	 * Depends of the format to use
	 */
	let outFile = baseFilePath + "." + FORMAT_TO_EXT[options.format];
	const bashCommands = [];
	const sql2bash = txt => txt.replace(/"/g, '\\"').replace(/\n/g, " ");
	const pgdb = `PG:"host=${CONFIG.PG_HOST} user=${CONFIG.PG_USER} dbname=${CONFIG.PG_DB_CLEAN} port=${CONFIG.PG_PORT}"`;

	// GeoJSON
	if(options.format === "geojson") {
		bashCommands.push(`ogr2ogr -f "GeoJSON" ${outFile} ${pgdb} -sql "${sql2bash(request)}" -t_srs EPSG:4326`);
	}
	// CSV
	else if(options.format === "csv") {
		bashCommands.push(`ogr2ogr -f "CSV" ${outFile} ${pgdb} -sql "${sql2bash(request)}" -t_srs EPSG:4326 -lco GEOMETRY=${options.aspoint ? "AS_XY" : "AS_WKT"} -lco SEPARATOR=SEMICOLON`);
	}
	// XLS
	else if(options.format === "xlsx") {
		bashCommands.push(`ogr2ogr -f "XLSX" ${outFile} ${pgdb} -sql "${sql2bash(request)}"`);
	}
	// Shapefile
	else if(options.format === "shapefile") {
		bashCommands.push(`mkdir -p ${baseFilePath}`);

		// Process geometry type by type
		if(options.aspoint) {
			const out = baseFilePath + "/data.shp";
			bashCommands.push(`ogr2ogr -f "ESRI Shapefile" ${out} ${pgdb} -sql "${sql2bash(request)}" -lco ENCODING=UTF-8`);
		}
		else {
			theme.geomtypes.forEach(type => {
				const out = baseFilePath + "/" + type + ".shp";
				const typeReq = request.replace(/<GEOMCONDTYPE>/g, GEOM_TO_COND[type]);
				bashCommands.push(`ogr2ogr -f "ESRI Shapefile" ${out} ${pgdb} -sql "${sql2bash(typeReq)}" -lco ENCODING=UTF-8`);
			});
		}

		// Zip the whole output folder
		bashCommands.push(`zip -j -r ${outFile} ${baseFilePath} && rm -rf ${baseFilePath}`);
	}

	// Add metadata along dataset
	if(options.metadata) {
		const zipFile = baseFilePath + ".zip";
		bashCommands.push(`zip -j ${zipFile} ${outFile} ${metadataFile} ${licenseFile}`);
		bashCommands.push(`printf "@ ${outFile.replace(CONFIG.WORK_DIR + "/", "")}\n@=data.${FORMAT_TO_EXT[options.format]}\n" | zipnote -w ${zipFile}`);
		bashCommands.push(`printf "@ ${metadataFile.replace(CONFIG.WORK_DIR + "/", "")}\n@=metadata.csv\n" | zipnote -w ${zipFile}`);
		bashCommands.push(`printf "@ ${licenseFile.replace(CONFIG.WORK_DIR + "/", "")}\n@=license.txt\n" | zipnote -w ${zipFile}`);
		bashCommands.push(`rm ${outFile} ${metadataFile} ${licenseFile}`);
		outFile = zipFile;
	}

	// Execute command on system
	return exec(bashCommands.join(" && "))
	.then(result => {
		// Send failure message if any
		if(result.stderr.length > 0) {
			// Check if it's not a warning
			if(
				result.stderr.split('\n').filter(l => (
					l.trim().length > 0
					&& !l.trim().startsWith('Warning')
					&& !l.trim().startsWith('This warning')
				)).length > 0
			) {
				throw new Error("Can't extract data from DB: "+result.stderr);
			}
		}

		if(options.destinationFolder) {
			const newFile = `${options.destinationFolder}/${theme.id}${options.metadata ? "_"+options.format+".zip" : "."+options.format}`;
			fs.copyFileSync(outFile, newFile);
			fs.unlinkSync(outFile);
			outFile = newFile;
		}

		return outFile;
	});
}

/**
 * Full dump of database of a given theme
 * @param {Object} theme The theme metadata
 * @param {Object} [options] Options
 * @param {string} [options.baseUrl] Base URL where files will be available at the end
 * @param {string} [options.destinationFolder] Folder which will receive processed files
 * @return {Promise} Resolves with output filenames
 */
exports.createDumpTheme = (theme, options) => {
	return whenAvailable()
	.then(() => {
		// Get request for this theme
		const tablename = "tmp_"+Date.now();
		const request = getSQLForThemeData(theme, null, {
			format: "table",
			tablename: tablename
		});
		const cleanupDb = () => pool.query(`DROP TABLE ${tablename}`);

		// Launch SQL request
		return pool.query(request)
		.then(() => {
			// Creates files
			const promises = [
				createDataFile(theme, null, `SELECT geom_full AS geom, * FROM ${tablename}`, {
					format: "geojson",
					aspoint: false,
					metadata: true,
					url: `${options.baseUrl}/${theme.id}_geojson.zip`,
					destinationFolder: options.destinationFolder
				}),
				createDataFile(theme, null, `SELECT geom_center AS geom, * FROM ${tablename}`, {
					format: "csv",
					aspoint: true,
					metadata: true,
					url: `${options.baseUrl}/${theme.id}_csv.zip`,
					destinationFolder: options.destinationFolder
				})
			];

			return Promise.all(promises)
			.then(outFiles => {
				return cleanupDb()
				.then(() => {
					return outFiles;
				});
			})
			.catch(e => {
				return cleanupDb()
				.then(() => { throw e; });
			});
		});
	});
};

/**
 * Extract thematic data
 * @param {Object} theme The theme metadata
 * @param {Object} boundary The boundary metadata, or null for whole database export
 * @param {Object} [options] Options
 * @param {string} [options.format] Output file format
 * @param {number} [options.radius] Radius around boundary center for data filtering
 * @param {boolean} [options.aspoint] Geometry will be converted as point, instead full geometry is used
 * @param {boolean} [options.metadata] Add a metadata file (SCDL format)
 * @param {string} [options.url] Original URL used for getting this dataset
 * @return {Promise} Resolves on output file
 */
exports.getThemeData = (theme, boundary, options) => {
	return whenAvailable()
	.then(() => {
		const start = Date.now();
		options.format = options.format || "csv";
		options.metadata = options.metadata || false;
		return createDataFile(theme, boundary, getSQLForThemeData(theme, boundary, options), options)
		.then(outfile => {
			console.log(`Request ${theme.id} on ${boundary ? boundary.name : "dump"} (${new Date().toISOString().split("T")[0]}): ${Math.round((Date.now()-start)/1000)}s`);
			return outfile;
		});
	});
};
