/*
 * Code for database-wide theme dumps
 */

const CONFIG = require('../../config.json');
const db = require('./db');
const themes = require('./themes');
const fs = require('fs');

// Create dump directory
const DUMP_DIR = __dirname + '/../dump';
if(!fs.existsSync(DUMP_DIR)) {
	fs.mkdirSync(DUMP_DIR, { recursive: true });
}

// Look if user set a precise theme to dump
const args = process.argv.slice(2);
let dumpTheme = args.length === 1 ? args[0] : null;

// Create dump for each theme
const nextTheme = () => {
	const theme = themes.full.shift();
	const start = Date.now();

	if(!theme) {
		console.log("Dumping all themes done");
		return;
	}

	if(theme.skipDump) {
		return nextTheme();
	}

	if(dumpTheme && dumpTheme !== theme.id) {
		return nextTheme();
	}

	db.createDumpTheme(theme, {
		baseUrl: (CONFIG.API_BASE_URL || "http://localhost:3000") + "/dump",
		destinationFolder: DUMP_DIR
	})
	.then(() => {
		console.log(`Dump for theme ${theme.id} took ${Math.round((Date.now()-start)/1000)}s`);
		nextTheme();
	});
};

console.log(dumpTheme ? "Dumping specified theme" : "Dumping all themes");
nextTheme();
