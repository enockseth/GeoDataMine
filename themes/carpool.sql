--METADATA={ "name:fr": "Covoiturage (schéma Étalab Lieux de covoiturage 0.2.1)", "name:en": "Carpool station", "theme:fr": "Transports", "keywords:fr": [ "covoiturage" ], "description:fr": "Lieux de covoiturage issus d'OpenStreetMap (amenity=parking + carpool=yes|designated ou amenity=car_pooling) au format Schéma Lieux covoiturage", "skipGeomCSV": true }
--GEOMETRY=point,polygon

SELECT
	COALESCE(t.tags->'ref:FR:BNCLC', CONCAT(<ADM8REF>, '-C-???')) AS id_lieu,
	COALESCE(t.tags->'ref', <OSMID>) AS id_local,
	t.tags->'name' AS nom_lieu,
	CASE WHEN t.tags->'addr:housenumber' IS NOT NULL AND t.tags->'addr:street' IS NOT NULL THEN concat(t.tags->'addr:housenumber', ' ', t.tags->'addr:street') END AS ad_lieu,
	<ADM8NAME> AS com_lieu,
	<ADM8REF> AS insee,
	CASE
		WHEN t.amenity = 'parking' AND t.tags->'park_ride' IN ('bus', 'yes') THEN 'Parking relais'
		WHEN (t.amenity = 'parking' AND t.tags->'carpool' = 'designated') OR t.amenity = 'car_pooling' THEN 'Aire de covoiturage'
		ELSE 'Parking'
	END AS type,
	split_part(t.tags->'osm_timestamp', 'T', 1) AS date_maj,
	CASE WHEN t.access IS NULL or t.access != 'no' THEN true ELSE false END AS ouvert,
	'OpenStreetMap' AS source,
	ST_X(ST_Transform(ST_Centroid(<GEOMEMBED>), 4326)) AS "Xlong",
	ST_Y(ST_Transform(ST_Centroid(<GEOMEMBED>), 4326)) AS "Ylat",
	t.tags->'capacity' AS nbre_pl,
	t.tags->'capacity:disabled' AS nbre_pmr,
	t.tags->'maxstay' AS duree,
	t.tags->'opening_hours' AS horaires,
	t.tags->'operator' AS proprio,
	CASE
		WHEN t.tags->'lit' = 'yes' THEN 'true'
		WHEN t.tags->'lit' = 'no' THEN 'false'
	END AS lumiere,
	COALESCE(t.tags->'description', t.tags->'note') AS comm,
	<GEOM>
FROM <TABLE>
WHERE ((t.amenity = 'parking' AND t.carpool IN ('yes', 'designated')) OR t.amenity = 'car_pooling') AND <GEOMCOND>
