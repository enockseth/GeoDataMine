--METADATA={ "name:fr": "Défibrillateur (schéma ARS DAE 1.0.0)", "name:en": "Automated external defibrillator", "theme:fr": "Santé", "keywords:fr": [ "défibrilateur", "aed", "dae" ], "description:fr": "Défibrillateurs extraits d'OpenStreetMap (emergency=defibrillator) au format Schéma DAE de l'ARS", "skipGeomCSV": true }
--GEOMETRY=point

SELECT
	t.tags->'ref:FR:GeoDAE' AS gid,
	t.tags->'name' AS nom,
	ST_Y(ST_Transform(<GEOMEMBED>, 4326)) AS lat_coor1,
	ST_X(ST_Transform(<GEOMEMBED>, 4326)) AS long_coor1,
	t.tags->'addr:housenumber' AS adr_num,
	t.tags->'addr:street' AS adr_voie,
	t.tags->'addr:postcode' AS com_cp,
	<ADM8REF> AS com_insee,
	<ADM8NAME> AS com_nom,
	CASE WHEN t.tags->'indoor' = 'yes' THEN 'Intérieur' ELSE 'Extérieur' END AS acc,
	CASE
		WHEN t.tags->'access' IN ('yes', 'public', 'permissive') THEN 'oui'
		WHEN t.tags->'access' IS NOT NULL THEN 'non'
		ELSE ''
	END AS acc_lib,
	CASE
		WHEN t.tags->'security_desk' = 'yes' THEN 'oui'
		WHEN t.tags->'security_desk' = 'no' THEN 'non'
		ELSE ''
	END AS acc_pcsec,
	CASE
		WHEN t.tags->'reception_desk' = 'yes' THEN 'oui'
		WHEN t.tags->'reception_desk' = 'no' THEN 'non'
		ELSE ''
	END AS acc_acc,
	t.tags->'level' AS acc_etg,
	t.tags->'defibrillator:location' AS acc_complt,
	CASE
		WHEN t.tags->'image' IS NOT NULL THEN t.tags->'image'
		WHEN t.tags->'mapillary' IS NOT NULL THEN concat('https://images.mapillary.com/', t.tags->'mapillary', '/thumb-2048.jpg')
	END AS photo1,
	'' AS photo2,
	CASE
		WHEN t.tags->'opening_hours' = '24/7' THEN '7j/7'
		ELSE ''
	END AS disp_j,
	CASE
		WHEN t.tags->'opening_hours' = '24/7' THEN '24h/24'
		ELSE ''
	END AS disp_h,
	CASE
		WHEN t.tags->'opening_hours' = '24/7' THEN ''
		WHEN t.tags->'opening_hours' IS NOT NULL THEN t.tags->'opening_hours'
		ELSE ''
	END AS disp_complt,
	COALESCE(t.tags->'contact:phone', t.tags->'phone') AS tel1,
	'' AS tel2,
	COALESCE(t.tags->'contact:email', t.tags->'email') AS site_email,
	t.tags->'start_date' AS date_instal,
	CASE
		WHEN t.tags->'operational_status' = 'operating' THEN 'En fonctionnement'
		WHEN t.tags->'operational_status' = 'out_of_order' THEN 'Hors service'
		WHEN t.tags->'operational_status' = 'closed' THEN 'Supprimé définitivement'
		WHEN t.tags->'operational_status' = 'short_term_absence' THEN 'Absent momentanément'
		ELSE 'Inconnu'
	END AS etat_fonct,
	t.tags->'manufacturer:ref:FR:SIREN' AS fab_siren,
	t.tags->'manufacturer' AS fab_rais,
	t.tags->'maintainer:ref:FR:SIREN' AS mnt_siren,
	t.tags->'maintainer' AS mnt_rais,
	t.tags->'defibrillator_model' AS modele,
	t.tags->'serial_number' AS num_serie,
	t.tags->'ref:IUD' AS id_euro,
	CASE
		WHEN t.tags->'pediatric_electrodes' = 'yes' THEN 'oui'
		WHEN t.tags->'pediatric_electrodes' = 'no' THEN 'non'
		ELSE ''
	END AS lc_ped,
	'' AS dtpr_lcped,
	'' AS dtpr_lcad,
	'' AS dtpr_bat,
	'' AS freq_mnt,
	CASE
		WHEN t.tags->'surveillance' = 'yes' THEN 'oui'
		WHEN t.tags->'surveillance' = 'no' THEN 'non'
		ELSE ''
	END AS dispsurv,
	t.tags->'last_maintenance:date' AS dermnt,
	COALESCE(t.tags->'operator:ref:FR:SIREN', t.tags->'ref:FR:SIREN') AS expt_siren,
	t.tags->'operator' AS expt_rais,
	t.tags->'operator:phone' AS expt_tel1,
	'' AS expt_tel2,
	t.tags->'operator:email' AS expt_email,
	<GEOM>
FROM <TABLE>
WHERE t.emergency = 'defibrillator' AND <GEOMCOND>
