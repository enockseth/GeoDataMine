--METADATA={ "name:fr": "Cinéma", "name:en": "Cinema", "theme:fr": "Culture", "keywords:fr": [ "cinema", "salles" ], "description:fr": "Salles de cinéma issues d'OpenStreetMap (amenity=cinema)" }
--GEOMETRY=point,polygon

SELECT
	<OSMID> as osm_id, t.tags->'ref:FR:CNC' AS ref_cnc,
	t.tags->'name' AS name,
	COALESCE(t.tags->'brand', t.tags->'operator') AS marque,
	t.wheelchair, t.tags->'opening_hours' AS opening_hours,
	t.tags->'open_air' AS open_air, t.tags->'drive_in' AS drive_in,
	t.tags->'cinema:3D' AS cinema3d, t.tags->'screen' AS nb_screens,
	t.tags->'capacity' AS capacity, t.tags->'acoustic' AS acoustic,
	COALESCE(t.tags->'website', t.tags->'contact:website', t.tags->'brand:website') AS website,
	COALESCE(t.tags->'phone', t.tags->'contact:phone') AS phone, COALESCE(t.tags->'facebook', t.tags->'contact:facebook') AS facebook,
	COALESCE(t.tags->'wikidata', t.tags->'brand:wikidata') AS wikidata, t.tags->'ref:FR:SIRET' AS siret,
	<ADM8REF> AS com_insee, <ADM8NAME> AS com_nom, <GEOM>
FROM <TABLE>
WHERE t.amenity = 'cinema' AND <GEOMCOND>
