--METADATA={ "name:fr": "Commerces", "name:en": "Shops", "theme:fr": "Entreprise et concurrence", "keywords:fr": [ "commerce", "services" ], "description:fr": "Commerces au sens large issus d'OpenStreetMap (shop=* ou craft=* ou office=educational_institution;employment_agency; estate_agent;guide;insurance;moving_company;private_investigator;travel_agent;visa;it ou amenity=bar;cafe;fast_food;food_court;ice_cream;pub;restaurant;driving_school;boat_rental;car_rental;car_wash;fuel;bank;bureau_de_change;pharmacy;cinema;nightclub;animal_boarding;animal_breeding;crematorium;internet_cafe;post_office;public_bath;pharmacy;vehicle_inspection)" }
--GEOMETRY=point,polygon

SELECT
	<OSMID> as osm_id, COALESCE(t.office, t.craft, t.amenity, t.shop) AS type, t.tags->'name' AS name,
	t.tags->'brand' AS brand, t.tags->'operator' AS operator, t.wheelchair,
	t.tags->'opening_hours' AS opening_hours, t.tags->'level' AS level, t.tags->'ref:FR:SIRET' AS siret,
	COALESCE(t.tags->'wikidata', t.tags->'brand:wikidata') AS wikidata,
	COALESCE(t.tags->'website', t.tags->'contact:website', t.tags->'brand:website') AS website,
	COALESCE(t.tags->'phone', t.tags->'contact:phone') AS phone,
	COALESCE(t.tags->'email', t.tags->'contact:email') AS email,
	COALESCE(t.tags->'facebook', t.tags->'contact:facebook') AS facebook,
	<ADM8REF> AS com_insee, <ADM8NAME> AS com_nom,
	CONCAT('https://www.caresteouvert.fr/@', ST_Y(ST_Transform(ST_Centroid(<GEOMEMBED>), 4326)), ',', ST_Y(ST_Transform(ST_Centroid(<GEOMEMBED>), 4326)), ',17/place/',substring(<OSMID>, 1, 1),  regexp_replace(<OSMID>, '.+/', '')) AS url_caresteouvert,
	<GEOM>
FROM <TABLE>
WHERE
	(
		(t.shop IS NOT NULL AND t.shop NOT IN ('fixme', 'vacant'))
		OR (t.craft IS NOT NULL AND t.craft NOT IN ('vacant', 'agricultural_engines'))
		OR t.office IN ('educational_institution', 'employment_agency', 'estate_agent', 'guide', 'insurance', 'moving_company', 'private_investigator', 'travel_agent', 'visa', 'it')
		OR t.amenity IN ('bar', 'cafe', 'fast_food', 'food_court', 'ice_cream', 'pub', 'restaurant', 'driving_school', 'boat_rental', 'car_rental', 'car_wash', 'fuel', 'bank', 'bureau_de_change', 'pharmacy', 'cinema', 'nightclub', 'animal_boarding', 'animal_breeding', 'crematorium', 'internet_cafe', 'post_office', 'public_bath', 'pharmacy', 'vehicle_inspection')
	)
	AND <GEOMCOND>
