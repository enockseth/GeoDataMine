--METADATA={ "name:fr": "Stations de taxi (schéma Etalab Stations de taxi 0.1.1)", "name:en": "Taxi stations", "theme:fr": "Transports", "keywords:fr": [ "taxi" ], "description:fr": "Emplacement d'attente pour les taxis issus d'OpenStreetMap (amenity=taxi)", "skipGeomCSV": true }
--GEOMETRY=point,polygon

SELECT
	COALESCE(t.tags->'ref', <OSMID>) AS id,
	t.tags->'name' AS nom,
	<ADM8REF> AS insee,
	CONCAT(ST_Y(ST_Transform(ST_Centroid(<GEOMEMBED>), 4326)), ', ', ST_X(ST_Transform(ST_Centroid(<GEOMEMBED>), 4326))) AS geopoint,
	array_to_string(array_remove(ARRAY[
		COALESCE(t.tags->'addr:housenumber', ''),
		COALESCE(t.tags->'addr:street', ''),
		COALESCE(t.tags->'addr:postcode', ''),
		COALESCE(t.tags->'addr:city', '')
	], ''), ' ') AS adresse,
	t.tags->'capacity' AS emplacements,
	COALESCE(t.tags->'phone', t.tags->'contact:phone') AS no_appel,
	COALESCE(t.tags->'description', t.tags->'note') AS info,
	<GEOM>
FROM <TABLE>
WHERE t.amenity = 'taxi' AND <GEOMCOND>
