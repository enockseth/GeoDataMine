--METADATA={ "name:fr": "Patrimoine historique", "name:en": "Historic features", "theme:fr": "Culture", "keywords:fr": [ "monument historique", "petit patrimoine", "église", "fontaine", "lavoir" ], "description:fr": "Patrimoine historique issu d'OpenStreetMap (historic=* ou amenity=fountain|place_of_worship|lavoir)" }
--GEOMETRY=point,polygon

SELECT
	<OSMID> as osm_id, COALESCE(t.historic, t.amenity) AS type, t.tags->'name' AS name, t.tags->'ref:mhs' AS ref_mhs,
	t.tags->'religion' AS religion, t.tags->'denomination' AS religion_denomination,
	t.tags->'inscription' AS inscription, t.tags->'start_date' AS build_date, t.tags->'description' AS description,
	t.tags->'wikipedia' AS wikipedia, t.tags->'historic:civilization' AS civilization, t.tags->'heritage' AS heritage,
	<ADM8REF> AS com_insee, <ADM8NAME> AS com_nom, <GEOM>
FROM <TABLE>
WHERE (t.amenity IN ('fountain', 'place_of_worship', 'lavoir') OR t.historic IS NOT NULL) AND <GEOMCOND>
