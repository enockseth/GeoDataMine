--METADATA={ "name:fr": "Aménagements cyclables (schéma Étalab Aménagements cyclables 0.3.3)", "name:en": "Cycleway", "theme:fr": "Transports", "keywords:fr": [ "piste cyclable", "bande cyclable", "aménagement cyclable" ], "description:fr": "Aménagement cyclables issues d'OpenStreetMap (cycleway=* ou cycleway:right=* ou cycleway:left=* ou cycleway:both=* ou highway=cycleway)" }
--GEOMETRY=line

SELECT
	COALESCE(t.tags->'ref', '') AS id_local,
	'' AS reseau_loc,
	array_to_string(array_remove(ARRAY[t.rel_tags->'route_icn_ref', t.rel_tags->'route_ncn_ref', t.rel_tags->'route_rcn_ref', t.rel_tags->'route_lcn_ref'], NULL), ' - ') AS nom_loc,
	<OSMID> AS id_osm,
	array_to_string(array_remove(ARRAY[t.rel_tags->'route_icn_ref', t.rel_tags->'route_ncn_ref', t.rel_tags->'route_rcn_ref', t.rel_tags->'route_lcn_ref'], NULL), ':') AS num_iti,
	<ADM8REF> AS code_com_d,
	CASE
		WHEN t.tags->'cyclestreet' = 'yes' THEN 'VELO RUE'

		WHEN t.tags->'ramp:bicycle' = 'yes' THEN 'RAMPE'

		WHEN t.highway IS NOT NULL AND t.highway != 'cycleway' AND t.cycleway = 'lane' AND t.tags->'lanes' = '1' AND (t.tags->'oneway' IS NULL OR t.tags->'oneway' = 'no') THEN 'CHAUSSEE A VOIE CENTRALE BANALISEE'

		WHEN t.bicycle = 'designated' AND ((t.highway = 'path' AND (t.tags->'motor_vehicle' IS NULL OR t.tags->'motor_vehicle' = 'no' OR t.tags->'motorcar' IS NULL OR t.tags->'motorcar' = 'no')) OR t.highway = 'footway' OR (t.highway = 'track' AND t.tags->'tracktype' = 'grade1')) THEN 'VOIE VERTE'

		WHEN t.bicycle = 'yes' AND ((t.highway = 'path' AND (t.tags->'motor_vehicle' IS NULL OR t.tags->'motor_vehicle' = 'no' OR t.tags->'motorcar' IS NULL OR t.tags->'motorcar' = 'no')) OR t.highway = 'footway') THEN 'AMENAGEMENT MIXTE PIETON VELO HORS VOIE VERTE'

		WHEN t.cycleway = 'share_busway' OR t."cycleway:right" = 'share_busway' OR t."cycleway:both" = 'share_busway' OR (t.highway='service' AND t.tags->'access' = 'no' AND (t.tags->'psv' IN ('yes', 'designated') OR t.tags->'bus' IN ('yes', 'designated')) AND t.bicycle IN ('yes', 'designated')) THEN 'COULOIR BUS+VELO'

		WHEN t.highway = 'cycleway' OR t.cycleway = 'track' OR t."cycleway:right" = 'track' OR t."cycleway:both" = 'track' THEN 'PISTE CYCLABLE'

		WHEN t.cycleway = 'lane' OR t."cycleway:right" = 'lane' OR t."cycleway:both" = 'lane' THEN 'BANDE CYCLABLE'

		WHEN t.cycleway IS NOT NULL OR t."cycleway:right" IS NOT NULL OR t."cycleway:both" IS NOT NULL OR t.highway = 'cycleway' THEN 'AUTRE'

		ELSE 'AUCUN'
	END AS ame_d,
	CASE
		WHEN t.tags->'maxspeed' = '30' AND (t.tags->'zone:maxspeed' = 'FR:30' OR t.tags->'source:maxspeed' = 'FR:zone30') THEN 'ZONE 30'
		WHEN t.highway IN ('footway', 'pedestrian') THEN 'AIRE PIETONNE'
		WHEN t.highway = 'living_street' OR t.tags->'living_street' = 'yes' THEN 'ZONE DE RENCONTRE'
		WHEN t.tags->'maxspeed' ~ '^\d+$' AND (t.tags->'maxspeed')::int <= 50 THEN 'EN AGGLOMERATION'
		WHEN t.tags->'maxspeed' ~ '^\d+$' AND (t.tags->'maxspeed')::int > 50 THEN 'HORS AGGLOMERATION'
		ELSE ''
	END AS regime_d,
	CASE
		WHEN
			(t.highway = 'cycleway' AND (t.tags->'oneway' IS NULL OR t.tags->'oneway' = 'no'))
			OR (t."cycleway:right" IN ('lane', 'track') AND t.tags->'cycleway:right:oneway' = 'no')
			OR (t.highway = 'footway' AND t.tags->'bicycle' IN ('yes', 'designated') AND (t.tags->'oneway' IS NULL OR t.tags->'oneway' = 'no'))
			OR t.tags->'ramp:bicycle' = 'yes'
			OR (((t.highway = 'path' AND (t.tags->'motor_vehicle' IS NULL OR t.tags->'motor_vehicle' = 'no' OR t.tags->'motorcar' IS NULL OR t.tags->'motorcar' = 'no')) OR t.highway='footway') AND t.bicycle IN ('yes', 'designated') AND (t.tags->'oneway' IS NULL OR t.tags->'oneway' = 'no'))
		THEN 'BIDIRECTIONNEL'
		WHEN
			t.tags->'cyclestreet' = 'yes'
			OR t.tags->'ramp:bicycle' = 'yes'
			OR (t.cycleway IS NOT NULL AND t.cycleway != 'no')
			OR (t."cycleway:both" IS NOT NULL AND t."cycleway:both" != 'no')
			OR (t."cycleway:right" IS NOT NULL AND t."cycleway:right" != 'no')
			OR t.bicycle IN ('yes', 'designated')
			OR t.highway = 'cycleway'
			OR t.tags->'sidewalk:bicycle' = 'yes'
		THEN 'UNIDIRECTIONNEL'
		ELSE ''
	END AS sens_d,
	COALESCE(t.tags->'cycleway:right:width', t.tags->'cycleway:right:est_width', t.tags->'cycleway:both:width', t.tags->'cycleway:both:est_width', t.tags->'cycleway:width', t.tags->'cycleway:est_width', '') AS largeur_d,
	CASE
		WHEN
			t.highway IN ('footway', 'cycleway')
			OR t.tags->'sidewalk:bicycle' = 'yes'
			OR t.tags->'ramp:bicycle' = 'yes'
			OR (t.cycleway = 'track' OR t."cycleway:both" = 'track' OR t."cycleway:right" = 'track')
		THEN 'TROTTOIR'
		WHEN
			t.tags->'cyclestreet' = 'yes'
			OR (t.cycleway IS NOT NULL AND t.cycleway != 'no')
			OR (t."cycleway:both" IS NOT NULL AND t."cycleway:both" != 'no')
			OR (t."cycleway:right" IS NOT NULL AND t."cycleway:right" != 'no')
			OR t.bicycle IN ('yes', 'designated')
		THEN 'CHAUSSEE'
		ELSE ''
	END AS local_d,
	CASE
		WHEN t.highway='construction' OR t.tags->'construction' IS NOT NULL THEN 'EN TRAVAUX'
		ELSE 'EN SERVICE'
	END AS statut_d,
	CASE
		WHEN
			COALESCE(t.tags->'cycleway:right:surface', t.tags->'cycleway:surface', t.tags->'surface:right', t.tags->'surface') IN ('compacted', 'fine_gravel', 'grass_paver', 'chipseal', 'sett', 'cobblestone', 'unhewn_cobblestone')
			AND COALESCE(t.tags->'cycleway:right:smoothness', t.tags->'cycleway:smoothness', t.tags->'smoothness:right', t.tags->'smoothness') IN ('excellent', 'good')
			THEN 'RUGUEUX'
		WHEN
			COALESCE(t.tags->'cycleway:right:surface', t.tags->'cycleway:surface', t.tags->'surface:right', t.tags->'surface') IN ('asphalt', 'concrete', 'paved', 'concrete:lanes', 'concrete:plates', 'paving_stones', 'metal', 'wood')
			AND COALESCE(t.tags->'cycleway:right:smoothness', t.tags->'cycleway:smoothness', t.tags->'smoothness:right', t.tags->'smoothness') = 'bad'
			THEN 'RUGUEUX'
		WHEN
			COALESCE(t.tags->'cycleway:right:surface', t.tags->'cycleway:surface', t.tags->'surface:right', t.tags->'surface') IN ('compacted', 'fine_gravel', 'grass_paver', 'chipseal', 'sett', 'cobblestone', 'unhewn_cobblestone')
			AND COALESCE(t.tags->'cycleway:right:smoothness', t.tags->'cycleway:smoothness', t.tags->'smoothness:right', t.tags->'smoothness') = 'intermediate'
			THEN 'MEUBLE'
		WHEN
			COALESCE(t.tags->'cycleway:right:surface', t.tags->'cycleway:surface', t.tags->'surface:right', t.tags->'surface') IN ('unpaved', 'gravel', 'rock', 'pebblestone', 'ground', 'dirt', 'earth', 'grass', 'mud', 'sand', 'woodchips')
			AND COALESCE(t.tags->'cycleway:right:smoothness', t.tags->'cycleway:smoothness', t.tags->'smoothness:right', t.tags->'smoothness') IN ('excellent', 'good', 'intermediate')
			THEN 'MEUBLE'

		WHEN
			COALESCE(t.tags->'cycleway:right:smoothness', t.tags->'cycleway:smoothness', t.tags->'smoothness:right', t.tags->'smoothness') IN ('excellent', 'good')
			THEN 'LISSE'
		WHEN
			COALESCE(t.tags->'cycleway:right:smoothness', t.tags->'cycleway:smoothness', t.tags->'smoothness:right', t.tags->'smoothness') = 'intermediate'
			THEN 'RUGUEUX'
		WHEN
			COALESCE(t.tags->'cycleway:right:smoothness', t.tags->'cycleway:smoothness', t.tags->'smoothness:right', t.tags->'smoothness') IN ('bad', 'very_bad', 'horrible', 'very_horrible', 'impassable')
			THEN 'MEUBLE'

		WHEN
			COALESCE(t.tags->'cycleway:right:surface', t.tags->'cycleway:surface', t.tags->'surface:right', t.tags->'surface') IN ('asphalt', 'concrete', 'paved', 'concrete:lanes', 'concrete:plates', 'paving_stones', 'metal', 'wood')
			THEN 'LISSE'
		WHEN
			COALESCE(t.tags->'cycleway:right:surface', t.tags->'cycleway:surface', t.tags->'surface:right', t.tags->'surface') IN ('compacted', 'fine_gravel', 'grass_paver', 'chipseal', 'sett', 'cobblestone', 'unhewn_cobblestone')
			THEN 'RUGUEUX'
		WHEN
			COALESCE(t.tags->'cycleway:right:surface', t.tags->'cycleway:surface', t.tags->'surface:right', t.tags->'surface') IN ('unpaved', 'gravel', 'rock', 'pebblestone', 'ground', 'dirt', 'earth', 'grass', 'mud', 'sand', 'woodchips')
			THEN 'MEUBLE'

		WHEN
			COALESCE(t.tags->'cycleway:right:surface', t.tags->'cycleway:surface', t.tags->'surface:right', t.tags->'surface') IS NULL
			AND COALESCE(t.tags->'cycleway:right:smoothness', t.tags->'cycleway:smoothness', t.tags->'smoothness:right', t.tags->'smoothness') IN ('excellent', 'good')
			THEN 'LISSE'
		WHEN
			COALESCE(t.tags->'cycleway:right:surface', t.tags->'cycleway:surface', t.tags->'surface:right', t.tags->'surface') IS NULL
			AND COALESCE(t.tags->'cycleway:right:smoothness', t.tags->'cycleway:smoothness', t.tags->'smoothness:right', t.tags->'smoothness') = 'intermediate'
			THEN 'RUGUEUX'
		WHEN
			COALESCE(t.tags->'cycleway:right:surface', t.tags->'cycleway:surface', t.tags->'surface:right', t.tags->'surface') IS NULL
			AND COALESCE(t.tags->'cycleway:right:smoothness', t.tags->'cycleway:smoothness', t.tags->'smoothness:right', t.tags->'smoothness') IN ('bad', 'very_bad', 'horrible', 'very_horrible', 'impassable')
			THEN 'MEUBLE'

		WHEN
			COALESCE(t.tags->'cycleway:right:smoothness', t.tags->'cycleway:smoothness', t.tags->'smoothness:right', t.tags->'smoothness') IS NULL
			AND COALESCE(t.tags->'cycleway:right:surface', t.tags->'cycleway:surface', t.tags->'surface:right', t.tags->'surface') IN ('asphalt', 'concrete', 'paved', 'concrete:lanes', 'concrete:plates', 'paving_stones', 'metal', 'wood')
			THEN 'LISSE'
		ELSE ''
	END AS revet_d,
	<ADM8REF> AS code_com_g,
	CASE
		WHEN t.tags->'cyclestreet' = 'yes' AND (t.tags->'oneway' IS NULL OR t.tags->'oneway' = 'no') THEN 'VELO RUE'

		WHEN t.highway IS NOT NULL AND t.highway != 'cycleway' AND t.cycleway = 'lane' AND t.tags->'lanes' = '1' AND (t.tags->'oneway' IS NULL OR t.tags->'oneway' = 'no') THEN 'CHAUSSEE A VOIE CENTRALE BANALISEE'

		WHEN ((t.tags->'oneway' IS NULL OR t.tags->'oneway' = 'no') AND (t.tags->'junction' IS NULL OR t.tags->'junction' != 'roundabout') AND t.cycleway = 'share_busway') OR t."cycleway:left" = 'share_busway' OR t."cycleway:both" = 'share_busway' OR (t.highway='service' AND t.tags->'access' = 'no' AND (t.tags->'psv' IN ('yes', 'designated') OR t.tags->'bus' IN ('yes', 'designated')) AND t.bicycle IN ('yes', 'designated')) AND (t.tags->'oneway' IS NULL OR t.tags->'oneway' = 'no') THEN 'COULOIR BUS+VELO'

		WHEN t.highway IS NOT NULL AND t.highway != 'cycleway' AND t.tags->'oneway' = 'yes' AND t."cycleway:left" = 'track' THEN 'DOUBLE SENS CYCLABLE PISTE'

		WHEN t.highway IS NOT NULL AND t.highway != 'cycleway' AND t.tags->'oneway' = 'yes' AND (t."cycleway:left" = 'opposite_lane' OR t.cycleway = 'opposite_lane') THEN 'DOUBLE SENS CYCLABLE BANDE'

		WHEN t.highway IS NOT NULL AND t.highway != 'cycleway' AND t.tags->'oneway' = 'yes' AND t.tags->'oneway:bicycle' = 'no' THEN 'DOUBLE SENS CYCLABLE NON MATERIALISE'

		WHEN COALESCE(t.tags->'cycleway:left:segregated', t.tags->'cycleway:both:segregated', t.tags->'sidewalk:left:segregated') = 'no' OR (t.highway NOT IN ('footway', 'cycleway', 'path') AND COALESCE(t.tags->'cycleway:segregated', t.tags->'sidewalk:segregated', t.tags->'segregated') = 'no') THEN 'AMENAGEMENT MIXTE PIETON VELO HORS VOIE VERTE'

		WHEN ((t.tags->'oneway' IS NULL OR t.tags->'oneway' = 'no') AND (t.tags->'junction' IS NULL OR t.tags->'junction' != 'roundabout') AND t.cycleway = 'track') OR t."cycleway:left" = 'track' OR t."cycleway:both" = 'track' THEN 'PISTE CYCLABLE'

		WHEN ((t.tags->'oneway' IS NULL OR t.tags->'oneway' = 'no') AND (t.tags->'junction' IS NULL OR t.tags->'junction' != 'roundabout') AND t.cycleway = 'lane') OR t."cycleway:left" = 'lane' OR t."cycleway:both" = 'lane' THEN 'BANDE CYCLABLE'

		WHEN ((t.tags->'oneway' IS NULL OR t.tags->'oneway' = 'no') AND (t.tags->'junction' IS NULL OR t.tags->'junction' != 'roundabout') AND t.cycleway IS NOT NULL) OR t."cycleway:left" IS NOT NULL OR t."cycleway:both" IS NOT NULL THEN 'AUTRE'

		ELSE 'AUCUN'
	END AS ame_g,
	CASE
		WHEN t.tags->'maxspeed' = '30' AND (t.tags->'zone:maxspeed' = 'FR:30' OR t.tags->'source:maxspeed' = 'FR:zone30') THEN 'ZONE 30'
		WHEN t.highway = 'living_street' OR t.tags->'living_street' = 'yes' THEN 'ZONE DE RENCONTRE'
		WHEN t.tags->'maxspeed' ~ '^\d+$' AND (t.tags->'maxspeed')::int <= 50 THEN 'EN AGGLOMERATION'
		WHEN t.tags->'maxspeed' ~ '^\d+$' AND (t.tags->'maxspeed')::int > 50 THEN 'HORS AGGLOMERATION'
		ELSE ''
	END AS regime_g,
	CASE
		WHEN
			t.tags->'cycleway:left:oneway' = 'no'
		THEN 'BIDIRECTIONNEL'
		WHEN
			(t.tags->'cyclestreet' = 'yes' AND (t.tags->'oneway' IS NULL OR t.tags->'oneway' = 'no'))
			OR (t.highway IS NOT NULL AND t.highway != 'cycleway' AND t.cycleway = 'lane' AND t.tags->'lanes' = '1' AND (t.tags->'oneway' IS NULL OR t.tags->'oneway' = 'no'))
			OR (((t.tags->'oneway' IS NULL OR t.tags->'oneway' = 'no') AND (t.tags->'junction' IS NULL OR t.tags->'junction' != 'roundabout') AND t.cycleway = 'share_busway') OR t."cycleway:left" = 'share_busway' OR t."cycleway:both" = 'share_busway' OR (t.highway='service' AND t.tags->'access' = 'no' AND (t.tags->'psv' IN ('yes', 'designated') OR t.tags->'bus' IN ('yes', 'designated')) AND t.bicycle IN ('yes', 'designated')) AND (t.tags->'oneway' IS NULL OR t.tags->'oneway' = 'no'))
			OR (t.highway IS NOT NULL AND t.highway != 'cycleway' AND t.tags->'oneway' = 'yes' AND t."cycleway:left" = 'track')
			OR (t.highway IS NOT NULL AND t.highway != 'cycleway' AND t.tags->'oneway' = 'yes' AND (t."cycleway:left" = 'opposite_lane' OR t.cycleway = 'opposite_lane'))
			OR t.tags->'oneway:bicycle' = 'no'
			OR t.tags->'sidewalk:bicycle' = 'yes'
			OR ((t.tags->'oneway' IS NULL OR t.tags->'oneway' = 'no') AND (t.tags->'junction' IS NULL OR t.tags->'junction' != 'roundabout') AND t.cycleway IS NOT NULL) OR t."cycleway:left" IS NOT NULL OR t."cycleway:both" IS NOT NULL
		THEN 'UNIDIRECTIONNEL'
		ELSE ''
	END AS sens_g,
	COALESCE(t.tags->'cycleway:left:width', t.tags->'cycleway:left:est_width', t.tags->'cycleway:both:width', t.tags->'cycleway:both:est_width', t.tags->'cycleway:width', t.tags->'cycleway:est_width', '') AS largeur_g,
	CASE
		WHEN
			(t.highway IS NOT NULL AND t.highway != 'cycleway' AND t.tags->'oneway' = 'yes' AND t."cycleway:left" = 'track')
			OR (COALESCE(t.tags->'cycleway:left:segregated', t.tags->'cycleway:both:segregated', t.tags->'sidewalk:left:segregated') = 'no' OR (t.highway NOT IN ('footway', 'cycleway', 'path') AND COALESCE(t.tags->'cycleway:segregated', t.tags->'sidewalk:segregated', t.tags->'segregated') = 'no'))
			OR (t.cycleway = 'track' OR t."cycleway:left" = 'track' OR t."cycleway:both" = 'track')
		THEN 'TROTTOIR'
		WHEN
			(t.tags->'cyclestreet' = 'yes' AND (t.tags->'oneway' IS NULL OR t.tags->'oneway' = 'no'))
			OR (t.highway IS NOT NULL AND t.highway != 'cycleway' AND t.cycleway = 'lane' AND t.tags->'lanes' = '1' AND (t.tags->'oneway' IS NULL OR t.tags->'oneway' = 'no'))
			OR (t.highway IS NOT NULL AND t.highway != 'cycleway' AND t.tags->'oneway' = 'yes' AND (t."cycleway:left" = 'opposite_lane' OR t.cycleway = 'opposite_lane'))
			OR (t.highway IS NOT NULL AND t.highway != 'cycleway' AND t.tags->'oneway' = 'yes' AND t.tags->'oneway:bicycle' = 'no')
			OR (t.cycleway = 'lane' OR t."cycleway:left" = 'lane' OR t."cycleway:both" = 'lane')
			OR (t.cycleway IS NOT NULL OR t."cycleway:left" IS NOT NULL OR t."cycleway:both" IS NOT NULL)
		THEN 'CHAUSSEE'
		ELSE ''
	END AS local_g,
	CASE
		WHEN t.highway='construction' OR t.tags->'construction' IS NOT NULL THEN 'EN TRAVAUX'
		ELSE 'EN SERVICE'
	END AS statut_g,
	CASE
		WHEN
			COALESCE(t.tags->'cycleway:left:surface', t.tags->'cycleway:surface', t.tags->'surface:left', t.tags->'surface') IN ('compacted', 'fine_gravel', 'grass_paver', 'chipseal', 'sett', 'cobblestone', 'unhewn_cobblestone')
			AND COALESCE(t.tags->'cycleway:left:smoothness', t.tags->'cycleway:smoothness', t.tags->'smoothness:left', t.tags->'smoothness') IN ('excellent', 'good')
			THEN 'RUGUEUX'
		WHEN
			COALESCE(t.tags->'cycleway:left:surface', t.tags->'cycleway:surface', t.tags->'surface:left', t.tags->'surface') IN ('asphalt', 'concrete', 'paved', 'concrete:lanes', 'concrete:plates', 'paving_stones', 'metal', 'wood')
			AND COALESCE(t.tags->'cycleway:left:smoothness', t.tags->'cycleway:smoothness', t.tags->'smoothness:left', t.tags->'smoothness') = 'bad'
			THEN 'RUGUEUX'
		WHEN
			COALESCE(t.tags->'cycleway:left:surface', t.tags->'cycleway:surface', t.tags->'surface:left', t.tags->'surface') IN ('compacted', 'fine_gravel', 'grass_paver', 'chipseal', 'sett', 'cobblestone', 'unhewn_cobblestone')
			AND COALESCE(t.tags->'cycleway:left:smoothness', t.tags->'cycleway:smoothness', t.tags->'smoothness:left', t.tags->'smoothness') = 'intermediate'
			THEN 'MEUBLE'
		WHEN
			COALESCE(t.tags->'cycleway:left:surface', t.tags->'cycleway:surface', t.tags->'surface:left', t.tags->'surface') IN ('unpaved', 'gravel', 'rock', 'pebblestone', 'ground', 'dirt', 'earth', 'grass', 'mud', 'sand', 'woodchips')
			AND COALESCE(t.tags->'cycleway:left:smoothness', t.tags->'cycleway:smoothness', t.tags->'smoothness:left', t.tags->'smoothness') IN ('excellent', 'good', 'intermediate')
			THEN 'MEUBLE'

		WHEN
			COALESCE(t.tags->'cycleway:left:smoothness', t.tags->'cycleway:smoothness', t.tags->'smoothness:left', t.tags->'smoothness') IN ('excellent', 'good')
			THEN 'LISSE'
		WHEN
			COALESCE(t.tags->'cycleway:left:smoothness', t.tags->'cycleway:smoothness', t.tags->'smoothness:left', t.tags->'smoothness') = 'intermediate'
			THEN 'RUGUEUX'
		WHEN
			COALESCE(t.tags->'cycleway:left:smoothness', t.tags->'cycleway:smoothness', t.tags->'smoothness:left', t.tags->'smoothness') IN ('bad', 'very_bad', 'horrible', 'very_horrible', 'impassable')
			THEN 'MEUBLE'

		WHEN
			COALESCE(t.tags->'cycleway:left:surface', t.tags->'cycleway:surface', t.tags->'surface:left', t.tags->'surface') IN ('asphalt', 'concrete', 'paved', 'concrete:lanes', 'concrete:plates', 'paving_stones', 'metal', 'wood')
			THEN 'LISSE'
		WHEN
			COALESCE(t.tags->'cycleway:left:surface', t.tags->'cycleway:surface', t.tags->'surface:left', t.tags->'surface') IN ('compacted', 'fine_gravel', 'grass_paver', 'chipseal', 'sett', 'cobblestone', 'unhewn_cobblestone')
			THEN 'RUGUEUX'
		WHEN
			COALESCE(t.tags->'cycleway:left:surface', t.tags->'cycleway:surface', t.tags->'surface:left', t.tags->'surface') IN ('unpaved', 'gravel', 'rock', 'pebblestone', 'ground', 'dirt', 'earth', 'grass', 'mud', 'sand', 'woodchips')
			THEN 'MEUBLE'

		WHEN
			COALESCE(t.tags->'cycleway:left:surface', t.tags->'cycleway:surface', t.tags->'surface:left', t.tags->'surface') IS NULL
			AND COALESCE(t.tags->'cycleway:left:smoothness', t.tags->'cycleway:smoothness', t.tags->'smoothness:left', t.tags->'smoothness') IN ('excellent', 'good')
			THEN 'LISSE'
		WHEN
			COALESCE(t.tags->'cycleway:left:surface', t.tags->'cycleway:surface', t.tags->'surface:left', t.tags->'surface') IS NULL
			AND COALESCE(t.tags->'cycleway:left:smoothness', t.tags->'cycleway:smoothness', t.tags->'smoothness:left', t.tags->'smoothness') = 'intermediate'
			THEN 'RUGUEUX'
		WHEN
			COALESCE(t.tags->'cycleway:left:surface', t.tags->'cycleway:surface', t.tags->'surface:left', t.tags->'surface') IS NULL
			AND COALESCE(t.tags->'cycleway:left:smoothness', t.tags->'cycleway:smoothness', t.tags->'smoothness:left', t.tags->'smoothness') IN ('bad', 'very_bad', 'horrible', 'very_horrible', 'impassable')
			THEN 'MEUBLE'

		WHEN
			COALESCE(t.tags->'cycleway:left:smoothness', t.tags->'cycleway:smoothness', t.tags->'smoothness:left', t.tags->'smoothness') IS NULL
			AND COALESCE(t.tags->'cycleway:left:surface', t.tags->'cycleway:surface', t.tags->'surface:left', t.tags->'surface') IN ('asphalt', 'concrete', 'paved', 'concrete:lanes', 'concrete:plates', 'paving_stones', 'metal', 'wood')
		THEN 'LISSE'
		ELSE ''
	END AS revet_g,
	CASE
		WHEN t.tags->'smoothness' IN ('excellent', 'good') THEN 'ROLLER'
		WHEN t.tags->'smoothness' = 'intermediate' THEN 'VELO DE ROUTE'
		WHEN t.tags->'smoothness' = 'bad' THEN 'VTC'
		WHEN t.tags->'smoothness' IN ('very_bad', 'horrible', 'very_horrible') THEN 'VTT'
		ELSE ''
	END access_ame,
	substring(t.tags->'osm_timestamp', 1, 10) AS date_maj,
	COALESCE(t.tags->'maxspeed', '') AS trafic_vit,
	CASE
		WHEN t.tags->'lit' = 'yes' THEN 'true'
		WHEN t.tags->'lit' = 'no' THEN 'false'
		ELSE ''
	END AS lumiere,
	CASE
		WHEN t.tags->'start_date' IS NOT NULL THEN substring(t.tags->'start_date', 1, 4)
		ELSE ''
	END AS d_service,
	COALESCE(t.tags->'description', t.tags->'note', t.tags->'fixme', '') AS comm,
	'OpenStreetMap' AS source,
	'WGS84' AS project_c,
	COALESCE(t.tags->'source:geometry', t.tags->'source', '') AS ref_geo,
	<GEOM>
FROM <TABLE>
WHERE
	(
		(t.cycleway IS NOT NULL AND t.cycleway != 'no')
		OR t.highway = 'cycleway'
		OR (t."cycleway:right" IS NOT NULL AND t."cycleway:right" != 'no')
		OR (t."cycleway:left" IS NOT NULL AND t."cycleway:left" != 'no')
		OR (t."cycleway:both" IS NOT NULL AND t."cycleway:both" != 'no')
		OR (t.highway IN ('path', 'footway') AND t.bicycle IN ('yes', 'designated'))
		OR (t.highway = 'service' AND t.bicycle IN ('yes', 'designated') AND t.tags->'access' = 'no' AND (t.tags->'psv' IN ('yes', 'designated') OR t.tags->'bus' IN ('yes', 'designated')))
		OR (t.highway IN ('pedestrian', 'unclassified', 'residential', 'service', 'primary', 'secondary', 'tertiary') AND t.tags->'cyclestreet' = 'yes')
		OR (t.highway = 'steps' AND t.tags->'ramp:bicycle' = 'yes')
	)
	AND <GEOMCOND>
