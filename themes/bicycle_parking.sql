--METADATA={ "name:fr": "Parking à vélos (schéma Étalab Stationnement cyclable 1.1.0)", "name:en": "Bicycle parking", "theme:fr": "Transports", "keywords:fr": [ "vélo", "parking à vélos" ], "description:fr": "Lieux de stationnement pour vélos issus d'OpenStreetMap (amenity=bicycle_parking) selon le schéma Stationnement cyclable de transport.data.gouv.fr", "skipGeomCSV": true }
--GEOMETRY=point,polygon

SELECT
	COALESCE(t.tags->'ref', <OSMID>) AS id_local,
	<OSMID> AS id_osm,
	<ADM8REF> AS code_com,
	CONCAT('[', ST_X(ST_Transform(ST_Centroid(<GEOMEMBED>), 4326)), ',', ST_Y(ST_Transform(ST_Centroid(<GEOMEMBED>), 4326)), ']') AS coordonneesxy,
	t.tags->'capacity' AS capacite,
	t.tags->'capacity:cargo_bike' AS capacite_cargo,
	CASE
		WHEN t.tags->'bicycle_parking' IN ('wall_loops', 'tree') THEN 'ROUE'
		WHEN t.tags->'bicycle_parking' = 'bollard' THEN 'CADRE'
		WHEN t.tags->'bicycle_parking' IN ('stands', 'two_tier', 'two-tier', 'handlebar_holder') THEN 'CADRE ET ROUE'
	END AS type_accroche,
	CASE
		WHEN t.tags->'bicycle_parking' = 'stands' THEN 'ARCEAU'
		WHEN t.tags->'bicycle_parking' = 'wall_loops' THEN 'RATELIER'
		WHEN t.tags->'bicycle_parking' IN ('two_tier', 'two-tier') THEN 'RACK DOUBLE ETAGE'
		WHEN t.tags->'bicycle_parking' = 'tree' THEN 'CROCHET'
		WHEN t.tags->'bicycle_parking' = 'handlebar_holder' THEN 'SUPPORT GUIDON'
		WHEN t.tags->'bicycle_parking' = 'bollard' THEN 'POTELET'
		WHEN t.tags->'bicycle_parking' = 'floor' THEN 'AUCUN EQUIPEMENT'
		ELSE 'AUTRE'
	END AS mobilier,
	CASE
		WHEN t.tags->'access' IN ('private', 'designated', 'customers') THEN 'PRIVE'
		WHEN t.tags->'access' IS NULL OR t.tags->'access' IN ('yes', 'public') THEN 'LIBRE ACCES'
	END AS acces,
	CASE
		WHEN t.tags->'fee' IS NULL OR t.tags->'fee' = 'no' THEN 'true'
		ELSE 'false'
	END AS gratuit,
	CASE
		WHEN t.tags->'bicycle_parking' = 'shed' THEN 'CONSIGNE COLLECTIVE FERMEE'
		WHEN t.tags->'bicycle_parking' = 'lockers' THEN 'BOX INDIVIDUEL FERME'
		ELSE 'AUTRE'
	END AS protection,
	CASE
		WHEN t.tags->'covered' IS NULL OR t.tags->'covered' = 'no' THEN 'false'
		ELSE 'true'
	END AS couverture,
	CASE
		WHEN t.tags->'surveillance' IS NULL OR t.tags->'surveillance' = 'no' THEN 'false'
		ELSE 'true'
	END AS surveillance,
	CASE
		WHEN t.tags->'lit' = 'no' THEN 'false'
		WHEN t.tags->'lit' = 'yes' THEN 'true'
	END AS lumiere,
	COALESCE(t.tags->'contact:website', t.tags->'operator:website', t.tags->'website') AS url_info,
	t.tags->'start_date' AS d_service,
	COALESCE(t.tags->'source', 'OpenStreetMap') AS source,
	t.tags->'owner' AS proprietaire,
	t.tags->'operator' AS gestionnaire,
	split_part(t.tags->'osm_timestamp', 'T', 1) AS date_maj,
	COALESCE(t.tags->'description', t.tags->'note', t.tags->'fixme') AS commentaires,
	<GEOM>
FROM <TABLE>
WHERE t.amenity = 'bicycle_parking' AND <GEOMCOND>
